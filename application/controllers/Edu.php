<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Edu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Edu_model');
    }

    public function index()
    {
        $data['title'] = 'Edu Consultant';
        $data['edu'] = $this->Edu_model->get_data();
        // var_dump($data);
        // die;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('edu/index', $data);
        $this->load->view('templates/footer');
    }

    public function detail($user)
    {

        // $user = $this->uri->segment(3);
        $tanggalawal = $this->input->get('tanggalawal');
        $tanggalakhir = $this->input->get('tanggalakhir');

        $data['title'] = 'Detail Edu Consultant';
        $data['edudetail'] = $this->Edu_model->getDataEduByUsername($user);
        $data['edu'] = $this->Edu_model->getJumlahStatusByUsername($user);
        $data['mahasiswa'] = $this->Edu_model->getDataMahasiswaByUsername($user, $tanggalawal, $tanggalakhir);
        // var_dump($data['mahasiswa']);
        // die;
        $data['edutotal'] = $this->Edu_model->getTotalDataByUsername($user);

        // var_dump($data);
        // die;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('edu/detail', $data);
        $this->load->view('templates/footer');
    }
}
