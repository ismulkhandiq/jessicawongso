<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . 'third_party/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class Pameran extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pameran_model');
    }

    public function index()
    {
        $tanggalawal = $this->input->get('tanggalawal');
        $tanggalakhir = $this->input->get('tanggalakhir');

        $data['title'] = 'Edu Pameran';
        $data['pameran'] = $this->Pameran_model->get_data($tanggalawal, $tanggalakhir);
        // var_dump($data);
        // die;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('pameran/index', $data);
        $this->load->view('templates/footer');
    }

    public function importdata()
    {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'xlsx|xls';
        $config['file_name'] = 'doc' . time();
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('importexcel')) {
            $file = $this->upload->data();
            $reader = ReaderEntityFactory::createXLSXReader();

            $reader->open('uploads/' . $file['file_name']);
            foreach ($reader->getSheetIterator() as $sheet) {
                $numRow = 1;
                foreach ($sheet->getRowIterator() as $row) {
                    if ($numRow > 1) {
                        $pameran = array(
                            'nama'          => $row->getCellAtIndex(1),
                            'asal_sekolah'  => $row->getCellAtIndex(2),
                            'no_wa'         => $row->getCellAtIndex(3),
                            'date_created'  => time(),
                        );
                        $this->Pameran_model->import_data($pameran);
                    }
                    $numRow++;
                }
                $reader->close();
                unlink('uploads/' . $file['file_name']);
                $this->session->set_flashdata('pesan', 'import Data Berhasil');
                redirect('pameran/index');
            }
        } else {
            echo "Error :" . $this->upload->display_errors();
        };
    }
}
