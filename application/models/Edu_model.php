<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Edu_model extends CI_Model
{
    public function get_data()
    {
        $query = $this->db->query('SELECT * FROM adis_sys_usr WHERE aktif_edu = 1');
        return $query->result_array();
        // return $this->db->get($table);
    }
    public function getDataEduByUsername($user)
    {
        $query = $this->db->query("SELECT * FROM adis_sys_usr WHERE username = '$user'");
        return $query->row_array();
    }

    public function getJumlahStatusByUsername($user)
    {
        $query = $this->db->query("
        SELECT s.username, count(a.ods), i.status FROM smart_telemarketing a 
        join adis_sys_usr s on s.username = a.edu
        join idx_status_invent i on i.id = a.ods
        WHERE s.username = '$user'
        group by a.ods, s.username");

        return $query->result_array();
    }

    public function getDataMahasiswaByUsername($user, $tanggalawal, $tanggalakhir)
    {
        // $tanggalawalbaru = strtotime($tanggalawal);
        // $tanggalakhirbaru = strtotime($tanggalakhir);
        // var_dump($tanggalakhirbaru, $tanggalawalbaru);
        // die;

        // $this->db->where('date_created >=', $tanggalawal);
        // $this->db->where('date_created <=', $tanggalakhir);
        // $where = $this->db->query(" and date_created >= '$tanggalawal' and date_created <= '$tanggalakhir'");

        // $query = $this->db->query("
        // SELECT s.username, a.kode, i.status, a.ket_ods, a.date_created, a.date_updated 
        // FROM smart_telemarketing a 
        // join adis_sys_usr s on s.username = a.edu 
        // join idx_status_invent i on i.id = a.ods
        // WHERE s.username = '$user' 
        // ");
        $this->db->select("s.username, a.kode, i.status, a.ket_ods, a.date_created, a.date_updated ");
        $this->db->from("smart_telemarketing as a");
        $this->db->join('adis_sys_usr as s', 's.username = a.edu');
        $this->db->join('idx_status_invent as i', 'i.id = a.ods');
        $this->db->where('s.username', $user);


        if (!empty($tanggalawal && $tanggalakhir)) {
            $where = "date_created between '$tanggalawal 00:00:00' and '$tanggalakhir 23:59:59'";
            $this->db->where($where);
            // var_dump($where);
            // die;
            // $query .= " and date_created between '$tanggalawal' and '$tanggalakhir'";
        }
        // var_dump($this->db->get_compiled_select());
        // die;
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTotalDataByUsername($user)
    {
        $query = $this->db->query("
        SELECT s.username, count(a.ods), i.status FROM smart_telemarketing a 
        join adis_sys_usr s on s.username = a.edu 
        join idx_status_invent i on i.id = a.ods 
        WHERE s.username = '$user'
        ");

        return $query->row_array();
    }
}
