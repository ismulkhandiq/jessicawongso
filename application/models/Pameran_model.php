<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pameran_model extends CI_Model
{
    public function get_data($tanggalawal = null, $tanggalakhir = null)
    {
        // $query = $this->db->query('SELECT * FROM pameran');
        // return $query->result_array();

        // append H:m:s


        if ($tanggalawal && !$tanggalakhir) {
            $tanggalakhir = $tanggalawal;
        }

        $tanggalawal = $tanggalawal ? strtotime($tanggalawal . " 00:00:00") : null;
        $tanggalakhir = $tanggalakhir ? strtotime($tanggalakhir . " 23:59:59") : null;


        if ($tanggalawal) {
            $this->db->where('date_created >=', $tanggalawal);
        }
        if ($tanggalakhir) {
            $this->db->where('date_created <=', $tanggalakhir);
        }



        return $this->db->get('pameran')->result_array();
    }

    public function import_data($pameran)
    {
        $jumlah = count($pameran);
        if ($jumlah > 0) {
            $this->db->replace('pameran', $pameran);
        }
    }
}
