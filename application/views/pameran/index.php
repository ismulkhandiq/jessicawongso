<?= $this->session->flashdata('pesan'); ?>

<div class="card">
    <div class="card-header">
        <!-- <h3 class="card-title">DATA EC</h3> -->
        <?= form_open_multipart('pameran/importdata') ?>
        <div class="row">
            <div class="col-3">
                <input type="file" class="form-control-file" id="importexcel" name="importexcel" accept=".xlsx,.xls">
            </div>
            <div class="col-3">
                <button type="submit" class="btn btn-primary btn-sm">Import</button>
            </div>
        </div>
        <?= form_close(); ?>

    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form class="mb-3" method="get" action="">
            <div class="row">
                <div class="col-2">
                    <input type="date" class="form-control" name="tanggalawal">
                </div>
                <div class="col-2">
                    <input type="date" class="form-control" name="tanggalakhir">
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-primary">Cari</button>
                </div>
            </div>
        </form>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr class="text-center">
                    <th>No</th>
                    <th>Nama Lengkap</th>
                    <th>Asal Sekolah</th>
                    <th>NO WA</th>
                    <th>Tanggal</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($pameran as $pmr) : ?>
                    <tr>
                        <td class="text-center"><?= $no++ ?></td>
                        <td><?= $pmr['nama']; ?></td>
                        <td><?= $pmr['asal_sekolah']; ?></td>
                        <td><?= $pmr['no_wa']; ?></td>
                        <td><?= date('d F Y', $pmr['date_created']); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>