</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?= base_url('assets/templates') ?>/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url('assets/templates') ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url('assets/templates') ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/templates') ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/templates') ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url('assets/templates') ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/templates') ?>/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets/templates') ?>/dist/js/demo.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<!-- page script -->
<script>
    $(function() {
        var table1 = $("#example1").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });

        $('.dropdown-item').on('click', function() {
            var keyword = $(this).data('keyword');
            // alert(keyword)
            table1
                .column(2)
                .search(keyword)
                .draw();
        });
    });

    <?php if ($this->uri->segment(1) == 'edu') : ?>
        const config = {
            type: 'pie',
            data: {
                labels: <?= json_encode(array_column($edu, 'status')) ?>,
                datasets: [{
                    label: "online tutorial subjects",
                    data: <?= json_encode(array_column($edu, 'count(a.ods)')) ?>,
                    backgroundColor: ['yellow', 'aqua', 'pink', 'lightgreen', 'gold', 'lightblue', 'green', 'blue', 'brown', 'purple', 'denim', 'cyan'],
                    hoverOffset: 5
                }],
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Grafik Data Edu Consultant'
                    }
                }
            },
        };

        new Chart(
            document.getElementById('acquisitions'), config
        );
    <?php endif; ?>
</script>
</body>

</html>