<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 3 | Registration Page</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('assets/templates') ?>/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?= base_url('assets/templates') ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('assets/templates') ?>/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition register-page">
    <div class="register-box">
        <div class="register-logo">
            <a href="../../index2.html"><b>Admin</b>LTE</a>
        </div>

        <div class="card">
            <div class="card-body register-card-body">
                <p class="login-box-msg"><strong>DAFTAR HADIR PAMERAN</strong></p>

                <form action="../../index.html" method="post">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Full name">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Retype password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-4" style="margin: auto;">
                            <button type="submit" class="btn btn-primary btn-block">Register</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
            <!-- /.form-box -->
        </div><!-- /.card -->
    </div>
    <!-- /.register-box -->

    <!-- jQuery -->
    <script src="<?= base_url('assets/templates') ?>/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?= base_url('assets/templates') ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url('assets/templates') ?>/dist/js/adminlte.min.js"></script>
</body>

</html>














<!-- <div class="container">
    <div class="row">
        <!-- <div class="col-lg-6">
				        <div class="hs-text">
						<img src="{$host}assets/landpage/img/elpe4febremove.png" alt="">
					</div> 
				</div> -->
<!-- <div class="col-lg-6 enamesde">
            <form id="form_reg" class="hero-form" method="post">
                <section class="score-section text-white set-bg">
                    <h3 style="color :#000; margin-bottom:10px;">Daftar Hadir Pameran</h3>
                </section>
                <input type="text" name="nama" placeholder="Nama Lengkap">
                <input type="email" name="email" placeholder="Alamat E-mail">
                <input type="text" name="kota" placeholder="Asal Kota">
                <input type="text" name="asal_sekolah" id="asal_sekolah" placeholder="Asal SMA / SMK Sederajat">
                <input type="text" name="wa" placeholder="No WhatsApp">
                <input type="hidden" name="sumber" placeholder="Sumber Iklan" value='{$campign}'>
                <button class="site-btn" type="submit" id="submit_btn_">Kirim</button>
            </form>
            <!-- <datalist id="asal_sekolahs">
                {foreach from=$daftarSekolah item=r}
                <option value="{$r.nama_sekolah}">{$r.nama_sekolah}</option>
                {/foreach}
            </datalist> -->
<!-- </div>
    </div>
</div>  -->