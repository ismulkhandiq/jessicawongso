<div class="card">
    <div class="card-body">
        <table>
            <tbody>
                <tr>
                    <td width="310">Hasil data dari EC :</td>
                    <td><strong>"<?= $edudetail['username']; ?>"</strong></td>
                </tr>
                <tr>
                    <td>Total semua data yang didapat :</td>
                    <td><strong>"<?= $edutotal['count(a.ods)']; ?>"<strong></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown">
                Status yang diperoleh
            </button>
            <div class="dropdown-menu">
                <?php
                foreach ($edu as $e) : ?>
                    <button class="dropdown-item" data-keyword="<?= $e['status']; ?>"><?= $e['status']; ?> : <strong><?= $e['count(a.ods)']; ?></strong></button>
                <?php endforeach; ?>
            </div>
        </div>
        <!-- <table>
            <tbody>
                <?php
                foreach ($edu as $e) : ?>
                    <tr>
                        <td width="350"><?= $e['status']; ?> :</td>
                        <td width="50"><?= $e['count(a.ods)']; ?></td>
                        <td><button class="dropdown-item" style="background-color: #696969;" data-keyword="<?= $e['status']; ?>">filter</button></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table> -->
        <hr>
        <div style="width: 400px;"><canvas id="acquisitions"></canvas></div>

    </div>
</div>
<div class="card">
    <div class="card-body">
        <form class="mb-3" method="get" action="">
            <div class="row">
                <div class="col-2">
                    <input type="date" class="form-control" name="tanggalawal">
                </div>
                <div class="col-2">
                    <input type="date" class="form-control" name="tanggalakhir">
                </div>
                <div class="col-2">
                    <button type="submit" class="btn btn-primary">Cari</button>
                </div>



            </div>
        </form>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Keterangan</th>
                    <th>Tanggal</th>
                    <th>Update</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($mahasiswa as $mhs) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $mhs['kode']; ?></td>
                        <td><?= $mhs['status']; ?></td>
                        <td><?= $mhs['ket_ods']; ?></td>
                        <td><?= $mhs['date_created']; ?></td>
                        <td><?= $mhs['date_updated']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>