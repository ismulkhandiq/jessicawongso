<div class="card">
    <div class="card-header">
        <h3 class="card-title">DATA EC</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr class="text-center">
                    <th>No</th>
                    <th>Fullname</th>
                    <th>Username</th>
                    <th>Kode</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($edu as $e) : ?>
                    <tr>
                        <td class="text-center"><?= $no++ ?></td>
                        <td><?= $e['fullname']; ?></td>
                        <td><?= $e['username']; ?></td>
                        <td><?= $e['kode']; ?></td>
                        <td><?= $e['email']; ?></td>
                        <td class="text-center">
                            <a href="<?= base_url('edu/detail/') . $e['username']; ?>" class="btn btn-primary btn-xs">Detail</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>